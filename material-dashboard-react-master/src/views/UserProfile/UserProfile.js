import React from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import CustomInput from "../../components/CustomInput/CustomInput.js";
import Button from "../../components/CustomButtons/Button.js";
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardAvatar from "../../components/Card/CardAvatar.js";
import CardBody from "../../components/Card/CardBody.js";
import CardFooter from "../../components/Card/CardFooter.js";
import ReactGA from 'react-ga';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import Search from "@material-ui/icons/Search";
import Add from "@material-ui/icons/Add";
import Table from "../../components/Table/Table.js";
import axiosInstance from "../../api/axios/index.js";

import avatar from "../../assets/img/faces/marc.jpg";

const styles = () => ({
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  },
  cardTitleDark: {
    color: "#000000",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
});

class UserProfile extends React.Component{
  constructor(props) {
		super(props);
		this.state = {
      query:'',
      similarIngredients:[],
      myRealIngredients:"",
      myIngredients:[],
      posiblePlates:[],
      class:''
    };
  };

  handleSearchSimilarIngredients(){
    let form = new FormData();
    if(this.state.query==''){
      return;
    }
    ReactGA.event({
      category: 'Ingredients_Textbox',
      action: 'ingredients',
      label: this.state.query
    });
    form.append('submit_ingr', this.state.query);
    form.append('list_ingr', this.state.myRealIngredients);
    console.log('text:',this.state.query);
    axiosInstance.post('/match_ingr', form, { headers:{
        'content-type': `multipart/form-data; boundary=${form._boundary}`,
    }})
    .then(({ data }) => {
      console.log(data);
      let l = []
      if(data['response']!=[]){
        data['response']['ingredients'].forEach(ingr => {
          l.push([ingr]);
        });
        let new_my_ingr_list = this.state.myIngredients 
        let new_my_real_ingr_list = this.state.myRealIngredients 
        
        if (!new_my_real_ingr_list.includes(this.state.query.toLowerCase())){
          new_my_ingr_list.push([this.state.query.toLowerCase()])
          new_my_real_ingr_list = new_my_real_ingr_list + " " + this.state.query.toLowerCase()
        }
        
        this.setState({similarIngredients: l,
          class: data['response']['clase'],
          myIngredients:new_my_ingr_list,
          myRealIngredients:new_my_real_ingr_list,
          posiblePlates: data['response']['similarPlates']});
        
      }

    }).catch(err => {
      console.error('Error: ', err)
    });
    
  }

  handleAddIngredients(ingre){
    let form = new FormData();
    if(ingre==''){
      return;
    }
    ReactGA.event({
      category: 'Add_Ingredients',
      action: 'ingredients',
      label: ingre
    });
    form.append('submit_ingr', this.state.query);
    form.append('list_ingr', this.state.myRealIngredients);

    axiosInstance.post('/match_ingr', form, { headers:{
        'content-type': `multipart/form-data; boundary=${form._boundary}`,
    }})
    .then(({ data }) => {
      console.log(data);
      let l = []
      if(data['response']!=[]){
        let new_my_ingr_list = this.state.myIngredients 
        let new_my_real_ingr_list = this.state.myRealIngredients 
        
        if (!new_my_real_ingr_list.includes(ingre.toLowerCase())){
          new_my_ingr_list.push([ingre.toLowerCase()])
          new_my_real_ingr_list = new_my_real_ingr_list + " " + ingre.toLowerCase()
        }
        
        this.setState({
          myIngredients:new_my_ingr_list,
          myRealIngredients:new_my_real_ingr_list,
          posiblePlates: data['response']['similarPlates']});
      }

    }).catch(err => {
      console.error('Error: ', err)
    });
    
  }

  handleSearch = (event) => {
    this.setState({query: event.target.value});
  }

  render(){
    ReactGA.initialize('UA-154453698-1');
    ReactGA.pageview("/ingredientes");
    const { classes } = this.props;
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={6}>
          <div className={classes.searchWrapper}>
            <a className={classes.block}>
              Ingresa tu ingrediente:<span>&nbsp;&nbsp;&nbsp;</span></a>
            <CustomInput
              formControlProps={{
                className: classes.margin + " " + classes.search
              }}
              inputProps={{
                onChange: this.handleSearch,
                placeholder: "Search",
                inputProps: {
                  "aria-label": "Search"
                },
              }}
              value={this.state.query}
            />
            <Button color="white" aria-label="edit" justIcon round onClick={() => {this.handleSearchSimilarIngredients()}}>
              <Search />
            </Button>
          </div>
          <Card>
            <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Clase: {this.state.class}</h4>
            </CardHeader>
            <CardBody>
            {this.state.similarIngredients.map((ingr) => {     
              console.log("Entered");                 
              // Return the element. Also pass key     
              return (
                <div style={{display: 'inline-block'}}>
                  <a className={classes.block}>{ingr[0]}<span>&nbsp;</span></a>
                  <Button color="white" aria-label="edit" justIcon round onClick={() => {this.handleAddIngredients(ingr[0])}}>
                    <Add />
                  </Button>
                  <a> <span>&nbsp;&nbsp;&nbsp;&nbsp;</span></a>
                  </div>
                ) 
            })}
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={6}>
          <Card>
            <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Mis ingredientes</h4>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="primary"
                tableHead={["Mi lista de ingredientes"]}
                tableData={this.state.myIngredients}
              />
            </CardBody>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
            <h4 className={classes.cardTitleWhite}>Platos semejantes</h4>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="primary"
                tableHead={["Nombre","clase","link","imagen"]}
                tableData={this.state.posiblePlates}
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
      
    );
  }
}

UserProfile.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserProfile);