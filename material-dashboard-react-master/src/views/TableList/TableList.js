import React,{TextInput} from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import Autocomplete from '@material-ui/lab/Autocomplete';
import TextField from '@material-ui/core/TextField';
// core components
import GridItem from "../../components/Grid/GridItem.js";
import GridContainer from "../../components/Grid/GridContainer.js";
import Table from "../../components/Table/Table.js";
import Card from "../../components/Card/Card.js";
import CardHeader from "../../components/Card/CardHeader.js";
import CardBody from "../../components/Card/CardBody.js";
import axiosInstance from "../../api/axios/index.js";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import ReactGA from 'react-ga';

const styles = () => ({
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
});

class TableList extends React.Component {
  constructor(props) {
		super(props);
		this.state = {
      query:'',
      similarPlates:[],
      allPlates:[]
    };
    
	};
  componentDidMount(){
    axiosInstance.get('/get_all_international_plates/')
    .then(({ data }) => {
      
      let l =[];
      data['response'].forEach(plate_info => {
        l.push({plato: plate_info})
      });
      this.setState({allPlates: l});
    });
  }
  handleSearchInternationalPLatesClick(value){
    let form = new FormData();
    let xx = ''
    if(value!=''){
      form.append('plate', value);
      xx = value;
    }else{
      form.append('plate', this.state.query);
      xx = this.state.query;
    }
    axiosInstance.post('/get_similar_international_plates/', form, { headers:{
        'content-type': `multipart/form-data; boundary=${form._boundary}`,
    }})
    .then(({ data }) => {
      //console.log(data);
      ReactGA.event({
        category: 'Internacional',
        action: 'Plate Search',
        label: xx
      });

      let l = []
      data['response'].forEach(plate_info => {
        l.push([plate_info['name'],plate_info['country'],plate_info['food_class'],plate_info['image_link'],plate_info['distance']])
      });
      this.setState({similarPlates: l});
      // return data;
    }).catch(err => {
      console.error('Error: ', err)
    });
    
  }

  handleSearch = (event) => {
    this.setState({query: event.target.value});
  }
  
  render(){
    ReactGA.initialize('UA-154453698-1');
    ReactGA.pageview("/internacional");
    const { classes } = this.props;
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <div className={classes.searchWrapper}>
            <a className={classes.block}>
              Ingresa tu plato:<span>&nbsp;&nbsp;&nbsp;</span></a>
            <Autocomplete
              id="combo-box-demo"
              options={this.state.allPlates}
              value={this.state.query}
              onChange={(event, newValue) => {
                if(newValue){
                  this.setState({query:newValue['plato']});
                  this.handleSearchInternationalPLatesClick(newValue['plato']);
                }
              }}
              onClick={(event)=>{
                if(event.target.toString()=="[object SVGSVGElement]"){
                  this.setState({query: ''});
                }
              }}
              getOptionLabel={option => option.plato}
              style={{ width: 400 }}
              renderInput={params => (
                <TextField {...params} 
                label={this.state.query} 
                variant="outlined" 
                fullWidth />
              )}
            />
          </div>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>Platos mas similares en el mundo</h4>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="primary"
                tableHead={["Plato", "Pais", "Clase", "Imagen","Distancia"]}
                tableData={this.state.similarPlates}
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}

TableList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TableList);