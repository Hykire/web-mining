from gensim.models import Word2Vec
import pickle
from scipy import spatial
from flask import Flask, request, send_from_directory
from flask import render_template
from werkzeug.wrappers import Request, Response
from flask import Flask, render_template, request, url_for
import pandas as pd
import numpy as np
from flask import jsonify
from flask_cors import CORS, cross_origin

def calc_w2vec(text):
    
    ingr_list = []
    
    ingr = text.split()
    for i in ingr:
        if i in w2vec_model.wv.vocab:
            ingr_list.append(i)        
    if len(ingr_list) > 0: 
        new_ingr = w2vec_model.wv.most_similar(positive=ingr_list,topn=10)
        new_ingr_list = []
        for n in new_ingr:
            new_ingr_list.append(n[0])
        #print(new_ingr_list)
        return new_ingr_list
    else: return ['no ingredients available']

#return mean of ingredients vector -> characteristic vector of a plate
def getFV(ingr_list):
    
    document = ""
    for ingr in ingr_list:
        document = document + " " + ingr
    
    words=document.split()
    s=np.zeros(100)
    k=1
    for w in words:
        if w in w2vec_model.wv.vocab:
            vector = w2vec_model.wv.get_vector(w)
            #normalized_vector = normalize_vector(vector)
            #s=s+normalized_vector
            s=s+vector
            k=k+1
    
    return s/k

def findMostSimilar(name):
    df=df_cat.copy()
    countries =  df_cat['country'].unique()
    input_plate = df.loc[df['name']==name]
    if len(input_plate)==0:
        return pd.DataFrame()
    input_plate_class = input_plate['food_class'].values[0]
    distances=[]
    indices=[]
    #print(input_plate['FV'].values[0])
    df['distance'] = 0
    final_df = pd.DataFrame(columns=df.columns)
    
    for j, country in enumerate(countries):
        min_distance = 1000
        min_idx = -1
        min_row = 0
        df_country = df[(df['country']==country) & (df['food_class']==input_plate_class)]
        for i,row in df_country.iterrows():
            print(row['FV'])
            print('AHHHHH')
            print([x for x in row['FV'][2:-1].split()])
            arr = [x for x in row['FV'][2:-1].split()]
            for elem in arr:
                print(elem)
                elem = float(elem)
            result = spatial.distance.cosine([float(x) for x in input_plate['FV'].values[0][2:-1].split()], [float(x) for x in row['FV'][2:-1].split()])
            if np.isnan(result):
                continue
            df.at[i,'distance']=result
            if result < min_distance:
                min_distance = result
                min_idx = i
                min_row = row
        final_df.loc[j] = min_row
        final_df.loc[j]['distance'] = min_distance
    final_df = final_df[final_df['country']!=0]  
    final_df.drop(['Unnamed: 0','FV','cleaned_ingredients_list'], axis=1, inplace=True)
    return final_df.sort_values(['distance'])

df_cat = None
w2vec_model = Word2Vec.load("word2vec.model")
knn_model = pickle.load(open('knn_model.sav', 'rb'))

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'application/json'

@app.route('/')
def begin():
    print("hello")
    return "hello"

@app.route('/get_similar_international_plates/', methods=['POST'])
def get_similar_international_plates():
    if request.form['plate']:
        print(request.form['plate'])
        results = findMostSimilar(request.form['plate'])
        if len(results)!=0:
            return jsonify(response=results.to_dict('records'))
    return jsonify(response=[])

@app.route('/get_all_international_plates/', methods=['GET'])
def get_all_plates():
    return jsonify(response=list(df_cat['name'].unique()))

@app.route('/match_ingr', methods=['POST'])
def match_ingr():
    if request.form['text']:
        text = request.form['text'].lower()
        fv = getFV([text])
        kn = knn_model.predict([fv])
        pred = kn[0]
        new_ingr = calc_w2vec(text)
        new_ingr = [n.replace('[','').replace(']','').replace(',','').replace("'",'')  for n in new_ingr]
        return jsonify(response={"ingredientes":new_ingr,"clase":pred})
    
    elif request.form['opt_ingr']:
        #new_ingr = request.form.getlist('ingr')
        #print(new_ingr)
        return 'hi'

if __name__ == '__main__':
    df_cat = pd.read_csv('./plates_vector.csv')
    app.run(host = '0.0.0.0',port=8000,debug=True)
