#!/usr/bin/env python
# coding: utf-8

#######################################
########## Scrapping ##################
#######################################

from bs4 import BeautifulSoup
import requests
import re
import os
encoding="utf-8"


url_recetasAlemanas = 'https://www.recetas.com/p/recetas-alemanas/'
url_recetasPeruanas = 'https://www.recetas.com/p/recetas-peruanas/'

def get_soup(url):
    
    content =  requests.get(url).content
    soup = BeautifulSoup(content,'lxml')
    
    return soup

#returns list of links to receipts for a given page url
def extract_links(url):
    
    soup = get_soup(url)
    div = soup.find('div', {'class' : 'page_list recipe_list'})
    entries = div.findAll('div', {'class' : 'show_entry_title'})
    links = [entry.find('a').get('href').replace('Ã¤', 'ä') for entry in entries]
    
    return links
 
#returns url of next page or None if the last page is reached
def get_next_page(url):
    
    soup = get_soup(url)
    pagination_div = soup.find('div', {'class' : 'pagination'})
    direction = pagination_div.findAll('a', {'class' : 'direction'})
    for entry in direction:
        if (entry.text == 'Siguiente'):
            return entry.get('href')
    return None
   
#starting with a page, extracts all links of all following pages
def extract_all_links(url, max_num_url):
    
    list_of_links = []
    new_url = url
    while(new_url is not None and len(list_of_links) < max_num_url):
        
        list_of_links = list_of_links + extract_links(new_url)
        new_url = get_next_page(new_url)
        
    return list_of_links
 
#returns list of food classes
def get_food_class(nav):
    
    food_class = []
    class_links = nav.findAll('a')
    
    for link in class_links[2:]:
        food_class.append(link.text)
        
    return food_class

def clean_html(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

#returns list of ingredients
def get_ingredients_list(ingredients):
  final =[]
  first = 1
  for ingredient in ingredients:
    aux = clean_html(ingredient).strip()
    if first:
      aux = aux[13:]
      first = 0
      
    if aux!='':
      final.append(aux.strip())
  
  return final

#cleans ingredients list from unnecessary information
def clean_ingredients(ingr_list):
    
    clean_ingredients = ""

    for ingr in ingr_list:
        #change to lower case
        aux = ingr.lower()
        #remove quantities etc.
        aux = re.sub(r'\d+ ?|k?gr?.? |gramos? ?|kilos? ?|lt. |litro |cdi?t?as?. ?|(c|d|m)l.? |oz. |tazas? |vasos? |pizcas? |uds?. |cc.? |lb. ', '', aux)
        aux = re.sub(r'xa0| a |de |^o |^y | o | y |en polvo ?|\/| en |gusto |para (\w* ?)*:?|,|\[|\]|\(|\)|\'|-|', '', aux)
        if len(aux) > 2:
            clean_ingredients = clean_ingredients + " " + aux

    return clean_ingredients

#gets plate data of a given country
def data_Plates_Countries(start_page_url):
    
    list_of_links = extract_all_links(start_page_url, 300)
    plates = []
    for url in list_of_links:
        
        plate = {}
        soup = get_soup(url)
        #print(url)

        #find image
        div_image = soup.find('div', {'class' : 'recipe_image'})
        #print(div_image)
        img = div_image.find('img')
        image_link = img.get('src')
        plate['image_link'] = image_link
        #print(image_link)
          
        #find name of receipt
        header = soup.find('h1').text
        plate['name'] = header
        #print(header)
        
        #find class of receipt
        nav = soup.find('nav', {'class' : 'path'})
        plate['food_class'] = get_food_class(nav)
        #print(food_class)
        
        #find country of receipt
        loc = soup.find('div', {'class' : 'location'})
        country = loc.find('a').text
        plate['country'] = country

        #find ingredients
        div_ingredients = soup.find('div', {'class' : 'ingredients_info'})
        ingredients = str(div_ingredients).split('<br/>')
        ingredients_list = get_ingredients_list(ingredients)
        plate['ingredients_list'] = ingredients_list
        #print(ingredients_list)
        
        #clean ingredients from unnecessary words
        clean_ingredients = clean_ingredients(ingredients_list)
        plate['cleaned_ingredients_list'] = clean_ingredients
        #print(clean_ingredients_list)

        plates.append(plate)

    return plates

#gets data of a given category
def data_Plates_Categories(start_page_url, category):
    
    list_of_links = extract_all_links(start_page_url, 300)
    plates = []
    for url in list_of_links:
        
        plate = {}
        soup = get_soup(url)
        #print(url)

        #add link
        plate['url'] = url

        #find image
        div_image = soup.find('div', {'class' : 'recipe_image'})
        #print(div_image)
        img = div_image.find('img')
        image_link = img.get('src')
        plate['image_link'] = image_link
        #print(image_link)
          
        #find name of receipt
        header = soup.find('h1').text
        plate['name'] = header
        #print(header)
        
        #aggregate given category
        plate['food_class'] = category
        #print(food_class)
        
        #find country of receipt
        loc = soup.find('div', {'class' : 'location'})
        country = loc.find('a')
        if (country is not None): plate['country'] = country.text
        else: plate['country'] = None    
        
        #find ingredients
        div_ingredients = soup.find('div', {'class' : 'ingredients_info'})
        ingredients = str(div_ingredients).split('<br/>')
        ingredients_list = get_ingredients_list(ingredients)
        plate['ingredients_list'] = ingredients_list
        #print(ingredients_list)
        
        #clean ingredients from unnecessary words
        clean_ingredients_list = clean_ingredients(ingredients_list)
        plate['cleaned_ingredients'] = clean_ingredients_list

        plates.append(plate)

    return plates

#import pickle

#with open('plates_germany.pickle', 'wb') as handle:
#    pickle.dump(plates_germany, handle, protocol=pickle.HIGHEST_PROTOCOL)
    
#with open('plates_peruvian.pickle', 'wb') as handle:
#    pickle.dump(plates_peru, handle, protocol=pickle.HIGHEST_PROTOCOL)

import json

#with open('plates_germany.json', 'w') as json_file:
#f  json.dump(plates_germany, json_file)

#with open('plates_peru.json', 'w') as json_file:
#  json.dump(plates_peru, json_file)

#######################################
########## List of Plates #############
#######################################

#plates_pan = data_Plates_Categories('https://www.recetas.com/recetas-panes/', 'Panes')

#plates_bebidas = data_Plates_Categories('https://www.recetas.com/recetas-bebidas/', 'Bebidas')

#plates_carnes = data_Plates_Categories('https://www.recetas.com/recetas-carnes/', 'Carnes')

#plates_ensaladas = data_Plates_Categories('https://www.recetas.com/recetas-ensaladas/', 'Ensaladas')

#platelist_tartas = data_Plates_Categories('https://www.recetas.com/recetas-tartas/', 'Tartas')

#platelist_sopas = data_Plates_Categories('https://www.recetas.com/recetas-sopas/', 'Sopas')

#platelist_mariscos = data_Plates_Categories('https://www.recetas.com/recetas-mariscos/', 'Mariscos')

#platelist_pizzas = data_Plates_Categories('https://www.recetas.com/recetas-pizzas/', 'Pizzas')

#platelist_categories = plates_pan + plates_bebidas + plates_carnes + plates_ensaladas + platelist_tartas + platelist_pizzas

#platelist_german = data_Plates_Countries(url_recetasAlemanas)

#platelist_peruvian = data_Plates_Countries(url_recetasPeruanas)

import pandas as pd
import numpy as np
from sklearn import preprocessing

#gets list of plates, creates dataframe out of given information
import json
def create_dataframe(platelist):
    
    json_plates = json.dumps(platelist)
    df = pd.read_json(json_plates)
    
    return df

#df_cat = create_dataframe(platelist_categories)
df_cat = pd.read_json(u'Dataframe.JSON')

#df_ger = create_dataframe(platelist_german)

#######################################
########## Word2Vector ################
#######################################

from gensim.test.utils import get_tmpfile
from gensim.models import word2vec

clean_ingredients = df_cat['cleaned_ingredients']
print(df_cat.head(5))

#create corpus and model

np.savetxt("ingredients.txt",clean_ingredients,fmt="%s",encoding="utf-8")
indgredient_info = word2vec.Text8Corpus("ingredients.txt")

model_w2v = word2vec.Word2Vec(indgredient_info,size=100,window=10,min_count=1)

model_w2v.save("word2vec.model")

#model_w2v.wv.most_similar(positive=["agua"],topn=10)

import math

#calculate length of vector
def mag(x): 
    return math.sqrt(sum(i**2 for i in x))

#normalize to unit vector
def normalize_vector(vector):
    return vector/mag(vector)

#return mean of ingredients vector -> characteristic vector of a plate
def getFV(document):
  
    words=document.split()
    s=np.zeros(100)
    k=1
    for w in words:
        if w in model_w2v.wv.vocab:
            vector = model_w2v.wv.get_vector(w)
            #normalized_vector = normalize_vector(vector)
            #s=s+normalized_vector
            s=s+vector
            k=k+1
    
    return s/k


#add column of characteristic vectors to dataframe
df_cat['FV'] = clean_ingredients.apply(lambda x: getFV(x))

#export dataframe
#with open("C:\\Users\\Inge\\Documents\\Webmining\\website\\Dataframe.JSON", "w+") as output_file:
#    output_file.write(df_cat.to_json())
df_cat.to_json('Dataframe.JSON')

#######################################
########## classification #############
#######################################

#gets dataframe, returns training set and test set as dataframes
def create_training_test_set(df):
    
    #randomize
    df_rand = df.sample(frac=1)
    
    #choose aprox. 80% for training
    row_num = len(df_rand.index)
    training_num = round(row_num * 0.8)
    
    #dataframes for tranining and testing
    df_train = df_rand[:training_num]
    df_test = df_rand[training_num:]
    
    return df_train, df_test

#training and test sets
training_test_set = create_training_test_set(df_cat)

training_set = training_test_set[0]
test_set = training_test_set[1]

X_train = training_set['FV'].values.tolist()
Y_train = training_set['food_class'].values.tolist()

X_test = test_set['FV'].values.tolist()
Y_test = test_set['food_class'].values.tolist()

y_test_asarray= np.asarray(Y_test)


#Random Forest
from sklearn.ensemble import RandomForestClassifier

# Create the model with 100 trees
rfmodel = RandomForestClassifier(n_estimators=100, 
                               bootstrap = True,
                               max_features = 'sqrt')
# Fit on training data
rfmodel.fit(X_train, Y_train)

# Actual class predictions
rf_predictions = rfmodel.predict(X_test)
# Probabilities for each class
rf_probs = rfmodel.predict_proba(X_test)[:, 1]


#KNN
from sklearn.neighbors import KNeighborsClassifier 

#train KNN
knn = KNeighborsClassifier(n_neighbors = 7).fit(X_train, Y_train)

# Actual class predictions
knn_predictions = knn.predict(X_test)

from sklearn.metrics import accuracy_score
print( "Acc for random forest: ",accuracy_score(y_test_asarray, rf_predictions))
print("Acc for KNN: ", accuracy_score(y_test_asarray, knn_predictions))

#test some vector 
#xc = X_test[10] 
#knn.predict([xc])

import pickle
from smart_open import open

pickle.dump(knn, open('knn_model.sav', 'wb'))
pickle.dump(rfmodel,  open('rf_model.sav', 'wb'))

###############################
###### similar plates #########
###############################
'''
from scipy import spatial

def findMostSimilar(name):
  df=df_cat.copy()
  goal = df.loc[df['name']==name]
  clase = goal['food_class'].values[0]
  print(clase)
  distances=[]
  indices=[]
  for i,row in df_cat.iterrows():
    result = spatial.distance.cosine(goal['FV'].values[0], row['FV'])
    df.at[i,'distance']=result
    
  df = df.sort_values(by ='distance')
  df = df.loc[df['country'].isin(['Alemania','Peru']) ]
  df = df.loc[df['food_class']==clase ]
  return df
'''