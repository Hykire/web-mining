
#######################################
######### website #####################
#######################################

import gensim
from gensim.test.utils import get_tmpfile
from gensim.models import word2vec
from gensim.models import Word2Vec
import pickle
import pandas as pd

w2vec_model = Word2Vec.load(u'word2vec.model')
rf_model = pickle.load(open(u'rf_model.sav', 'rb'))
dataframe = pd.read_json(u'Dataframe.JSON')

from flask import Flask, render_template, request, url_for
import pandas as pd
import numpy as np
import re

def calc_w2vec(ingr_list):

    #only use ingredients that are in w2v model
    ingr_in_model = []
    for i in ingr_list:
        if i in w2vec_model.wv.vocab:
            ingr_in_model.append(i)  

    #search top 20 similar indgredient 
    if len(ingr_in_model) > 0: 
        similar_ingr = w2vec_model.wv.most_similar(positive=ingr_in_model,topn=15)
        #only add similar ingredients that are not already in ingr_list, throw out duplicates
        similar_ingr_list = []
        for n in similar_ingr:
            if n[0] not in ingr_list: 
                similar_ingr_list.append(n[0])
        similar_ingr_list = set(similar_ingr_list)

        #print(new_ingr_list)
        return similar_ingr_list
    else: return []

#return mean of ingredients vector -> characteristic vector of a plate
def getFV(ingr_list):
    
    s=np.zeros(100)
    k=1
    for w in ingr_list:
        if w in w2vec_model.wv.vocab:
            vector = w2vec_model.wv.get_vector(w)
            #normalized_vector = normalize_vector(vector)
            #s=s+normalized_vector
            s=s+vector
            k=k+1
    
    return s/k

#find most similar plates to a plate vector
from math import*
 
def square_rooted(x):
 
    return round(sqrt(sum([a*a for a in x])),3)
 
def cosine_similarity(x,y):
 
    numerator = sum(a*b for a,b in zip(x,y))
    denominator = square_rooted(x)*square_rooted(y)
    return round(numerator/float(denominator),3)

def get_closest_plates(fv):

    aux_df = dataframe 
    aux_df['DistanceToVec'] = aux_df['FV'].apply(lambda x: cosine_similarity(fv, x))
    aux_df = aux_df.sort_values('DistanceToVec')
    most_similar = aux_df.head(5)

    ms_name = most_similar['name']
    ms_clas = most_similar['food_class']
    ms_url = most_similar['url']
    ms_pic = most_similar['image_link']

    return ms_name, ms_clas, ms_url, ms_pic


def add_new_ingr(ingr_str):
    #add data to session
    if not session.get('list_of_ingr', None):
        session['list_of_ingr'] = ingr_str
    else:
        session['list_of_ingr'] = session.get('list_of_ingr', None) + " " + ingr_str

    #calculate w2v for all ingredients added
    new_ingr = calc_w2vec(session.get('list_of_ingr', None))

    #predict type of plate
    ingr_list = session.get('list_of_ingr', None).split()
    fv = getFV(ingr_list)
    pred = rf_model.predict([fv])[0]

    #search closest plates
    most_similar = get_closest_plates(fv)
    plate_name = most_similar[0]
    plate_clas = most_similar[1]
    plate_url = most_similar[2]
    plate_img = most_similar[3]
        
    return render_template('match_ingr.html', classification=pred, ingr_list=ingr_list, new_ingr_list=new_ingr, 
        plate_descr=zip(plate_name, plate_url), plate_clas=plate_clas, plate_img=plate_img)

#calc_w2vec('harina agua')



from flask import Flask, request, send_from_directory, session, redirect
from flask import render_template
app = Flask(__name__,
            static_url_path='', 
            static_folder='static',
            template_folder='templates')

import random
app.secret_key = bytearray(random.getrandbits(8) for _ in range(16))
#b'7Lcu?\np-0"Rc/$gG'#random.getrandbits(16)


@app.route('/')
def startpage():
    return render_template('startpage.html')

@app.route('/match_ingr')
def show_match_ingr():
    return render_template('match_ingr.html')

@app.route('/match_ingr', methods=['POST'])
def match_ingr():
    if  request.form['submit_ingr']:
        ingr_str = request.form['submit_ingr']
        return add_new_ingr(ingr_str)
    
    elif request.form['check_ingr']:
        ingr_list = request.form.getlist('check_ingr')
        ingr_str = ""
        for i in ingr_list:
            ingr_str = ingr_str + " " + i
        return add_new_ingr(ingr_str)

    '''elif request.form['reset'] == "resetear":
        print("hi")
        #session['list_of_ingr'] = ""
        return render_template('match_ingr.html')'''

@app.route('/reset')
def reset():
    session['list_of_ingr'] = ""
    return redirect('http://35.196.86.56:8000/match_ingr')
            
@app.route('/similar_plates')
def sim_plate():
    return render_template('similar_plates.html')

if __name__ == '__main__':
    app.run(host= '0.0.0.0', port=8000)

#find closest number: min(myList, key=lambda x:abs(x-myNumber))
#sort db, find min entry
#check closest entries around it, return name, classification, link (add to db), image(?)

