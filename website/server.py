
#######################################
######### website #####################
#######################################

import gensim
from gensim.test.utils import get_tmpfile
from gensim.models import word2vec
from gensim.models import Word2Vec
import pickle
import pandas as pd
from scipy import spatial

w2vec_model = Word2Vec.load(u'word2vec.model')
rf_model = pickle.load(open(u'rf_model.sav', 'rb'))
dataframe = pd.read_json(u'Dataframe.JSON')

from flask import Flask, render_template, request, url_for, jsonify, session 
from flask_cors import CORS
import pandas as pd
import numpy as np
import re

def calc_w2vec(ingr_list):

    #only use ingredients that are in w2v model
    ingr_in_model = []
    for i in ingr_list:
        if i in w2vec_model.wv.vocab:
            ingr_in_model.append(i)  
    
    #search top 20 similar indgredient 
    if len(ingr_in_model) > 0: 
        similar_ingr = w2vec_model.wv.most_similar(positive=ingr_in_model,topn=15)
        #only add similar ingredients that are not already in ingr_list, throw out duplicates
        similar_ingr_list = []
        for n in similar_ingr:
            if n[0] not in ingr_list: 
                similar_ingr_list.append(n[0])
        similar_ingr_list = set(similar_ingr_list)

        #print(new_ingr_list)
        return similar_ingr_list
    else: return []

#return mean of ingredients vector -> characteristic vector of a plate
def getFV(ingr_list):
    
    s=np.zeros(100)
    k=1
    for w in ingr_list:
        if w in w2vec_model.wv.vocab:
            vector = w2vec_model.wv.get_vector(w)
            #normalized_vector = [i/sum(vector) for i in vector]
            #s=s+normalized_vector
            s=s+vector
            k=k+1
    
    return s/k

def getFV_norm(ingr_list):
    
    s=np.zeros(100)
    k=1
    for w in ingr_list:
        if w in w2vec_model.wv.vocab:
            vector = w2vec_model.wv.get_vector(w)
            normalized_vector = [i/sum(vector) for i in vector]
            s=s+normalized_vector
            #s=s+vector
            k=k+1
    
    return s/k
#find most similar plates to a plate vector
from math import*
 
def square_rooted(x):
 
    return round(sqrt(sum([a*a for a in x])),3)
 
def cosine_similarity(x,y):
    numerator = sum(a*b for a,b in zip(x,y))
    denominator = square_rooted(x)*square_rooted(y)
    return round(numerator/float(denominator),3)

def get_closest_plates(fv):

    aux_df = dataframe 
    aux_df['DistanceToVec'] = aux_df['FV'].apply(lambda x: cosine_similarity(fv, x))
    aux_df = aux_df.sort_values('DistanceToVec')
    most_similar = aux_df.head(5)

    ms_name = most_similar['name']
    ms_clas = most_similar['food_class']
    ms_url = most_similar['url']
    ms_pic = most_similar['image_link']

    return ms_name, ms_clas, ms_url, ms_pic


def add_new_ingr(ingr_str, lista_de_ingredientes):
    session['list_of_ingr'] = lista_de_ingredientes
    #add data to session
    if lista_de_ingredientes=="":
        session['list_of_ingr'] = ingr_str
    else:
        session['list_of_ingr'] = lista_de_ingredientes + " " + ingr_str

    print('list_of_ingr',session['list_of_ingr'])
    print('lista_de_ingredientes',lista_de_ingredientes)
    #calculate w2v for all ingredients added
    ingr_list = session.get('list_of_ingr', None).split(" ")
    new_ingr = calc_w2vec(ingr_list)
    #predict type of plate
    
    fv = getFV(ingr_list)
    print('fv',fv)
    pred = rf_model.predict([fv])[0]

    #search closest plates
    most_similar = get_closest_plates(getFV_norm(ingr_list))
    most_similar_ = []
    
    plate_name = list(most_similar[0])
    plate_clas = list(most_similar[1])
    plate_url = list(most_similar[2])
    plate_img = list(most_similar[3])
    
    for i in range(len(plate_name)):
        most_similar_.append([plate_name[i],plate_clas[i],plate_url[i],plate_img[i]])
    print('most_similar',most_similar_)

    return list(new_ingr), pred, most_similar_

def findMostSimilar(name):
    df=df_cat.copy()
    countries =  df_cat['country'].unique()
    input_plate = df.loc[df['name']==name]
    if len(input_plate)==0:
        return pd.DataFrame()
    input_plate_class = input_plate['food_class'].values[0]
    distances=[]
    indices=[]
    #print(input_plate['FV'].values[0])
    df['distance'] = 0
    final_df = pd.DataFrame(columns=df.columns)
    
    for j, country in enumerate(countries):
        min_distance = 1000
        min_idx = -1
        min_row = 0
        df_country = df[(df['country']==country) & (df['food_class']==input_plate_class)]
        for i,row in df_country.iterrows():
            arr = [x for x in row['FV'][2:-1].split()]
            for elem in arr:
                elem = float(elem)
            result = spatial.distance.cosine([float(x) for x in input_plate['FV'].values[0][2:-1].split()], [float(x) for x in row['FV'][2:-1].split()])
            if np.isnan(result):
                continue
            df.at[i,'distance']=result
            if result < min_distance:
                min_distance = result
                min_idx = i
                min_row = row
        final_df.loc[j] = min_row
        final_df.loc[j]['distance'] = min_distance
    final_df = final_df[final_df['country']!=0]  
    final_df.drop(['Unnamed: 0','FV','cleaned_ingredients_list'], axis=1, inplace=True)
    return final_df.sort_values(['distance'])

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'application/json'
import random
app.secret_key = bytearray(random.getrandbits(8) for _ in range(16))

@app.route('/')
def begin():
    return "hello"

@app.route('/reset', methods=['GET'])
def reset():
    session['list_of_ingr'] = ""
    return jsonify(response={})

@app.route('/get_similar_international_plates/', methods=['POST'])
def get_similar_international_plates():
    if request.form['plate']:
        results = findMostSimilar(request.form['plate'])
        if len(results)!=0:
            return jsonify(response=results.to_dict('records'))
    return jsonify(response=[])

@app.route('/get_all_international_plates/', methods=['GET'])
def get_all_plates():
    return jsonify(response=list(df_cat['name'].unique()))

@app.route('/match_ingr', methods=['POST'])
def match_ingr():
    
    if  request.form['submit_ingr']:
        ingr_str = request.form['submit_ingr']
        print(request.form)
        list_ing, pred, most_similar_ = add_new_ingr(ingr_str,request.form['list_ingr'])
        print(session['list_of_ingr'])
        return jsonify(response={"ingredients":list_ing,"clase":pred,"similarPlates":most_similar_})
    
    elif request.form['check_ingr']:
        ingr_list = request.form.getlist('check_ingr')
        ingr_str = ""
        for i in ingr_list:
            ingr_str = ingr_str + " " + i
        list_ing = add_new_ingr(ingr_str)
        print(list_ing)
        return jsonify(response={"ingredients":list_ing})
    return jsonify(response=[])

if __name__ == '__main__':
    df_cat = pd.read_csv('./plates_vector.csv')
    app.run(host = '0.0.0.0',port=8000,debug=True)
